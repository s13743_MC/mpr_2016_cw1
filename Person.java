package cw1;

import java.util.ArrayList;
import java.util.List;

public class Person {
	private String firstName;
	private String surname;
	private String phoneNumber;
	
	private List<Address> addresses = new ArrayList<Address>();
	private List<Role> roles = new ArrayList<Role>();
	
	public Person () {};
	
	public Person (String firstName, String surname, String phoneNumber, List<Address> addresses, List<Role> roles) {
		this.firstName = firstName;
		this.surname = surname;
		this.phoneNumber = phoneNumber;
		this.addresses = addresses;
		this.roles = roles;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public List<Address> getAddresses() {
		return addresses;
	}
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", surname=" + surname
				+ ", phoneNumber=" + phoneNumber + ", addresses=" + addresses
				+ ", roles=" + roles + "]";
	}
}
