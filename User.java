package cw1;

public class User {
	
	private String login;
	private String password;
	private Person person;
	
	public User() {};
	
	public User (String login, String password, Person person) {
		this.login = login;
		this.password = password;
		this.person = person;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "User [login=" + login + ", password=" + password + ", person="
				+ person + "]";
	}
}
