package cw1;

import java.util.ArrayList;
import java.util.List;

public class Role {
	
	private String roleName;
	
	private List<Permission> permissions = new ArrayList<Permission>();
	
	public Role () {};
	
	public Role (String roleName, List<Permission> permissions) {
		this.roleName = roleName;
		this.permissions = permissions;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	@Override
	public String toString() {
		return "Role [roleName=" + roleName + ", permissions=" + permissions
				+ "]";
	}
}
