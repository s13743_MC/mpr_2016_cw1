package cw1;

public class Address {
	
	private String userAddress;
	
	public Address () {};
	
	public Address (String userAddress) {
		this.userAddress = userAddress;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	@Override
	public String toString() {
		return "Address [userAddress=" + userAddress + "]";
	}
}
