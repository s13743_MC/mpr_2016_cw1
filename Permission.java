package cw1;

public class Permission {
	
	private String permissionName;
	
	public Permission () {};
	
	public Permission (String permissionName) {
		this.permissionName = permissionName;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	@Override
	public String toString() {
		return "Permission [permissionName=" + permissionName + "]";
	}
	
}
